<?php

/**
 * Plugin Name: my-fitness.blog
 * Description: Base plugin for my fitness blog
 * Version: 1.0
 * Author: Dennis Ploetner
 * License: GPL2
*/

require 'vendor/autoload.php';

$plugin_dir = basename( dirname( __FILE__ ) );
myfitnessblog\Plugin::init( $plugin_dir );
