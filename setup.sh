#!/usr/bin/env bash

echo -n "Enter mysql password for root and press [ENTER]: "
read -s PASS

mysql -u root --password=$PASS --execute="DROP DATABASE IF EXISTS wptest;"
bash bin/install-wp-tests.sh wptest root $PASS localhost latest
phpunit
