<?php

namespace myfitnessblog;

/**
 * Class Exercise
 *
 * @package myfitnessblog
 */
class Exercise extends PostType {

	const post_type = 'exercise';

	/**
	 * Get place holder title
	 */
	public function get_title_here() {
		return __( 'Insert exercise title here', 'myfitnessblog' );
	}

	/**
	 * Register post type
	 *
	 * @codeCoverageIgnore
	 */
	public function register() {
		$labels = [
			'name'                  => _x( 'Exercises', 'Post Type General Name', 'myfitnessblog' ),
			'singular_name'         => _x( 'Exercise', 'Post Type Singular Name', 'myfitnessblog' ),
			'menu_name'             => __( 'Exercises', 'myfitnessblog' ),
			'name_admin_bar'        => __( 'Exercise', 'myfitnessblog' ),
			'archives'              => __( 'Exercise archives', 'myfitnessblog' ),
			'attributes'            => __( 'Exercise attributes', 'myfitnessblog' ),
			'parent_item_colon'     => __( 'Parent item:', 'myfitnessblog' ),
			'all_items'             => __( 'All Exercises', 'myfitnessblog' ),
			'add_new_item'          => __( 'Add new exercise', 'myfitnessblog' ),
			'add_new'               => __( 'Add new', 'myfitnessblog' ),
			'new_item'              => __( 'New Exercise', 'myfitnessblog' ),
			'edit_item'             => __( 'Edit exercise', 'myfitnessblog' ),
			'update_item'           => __( 'Update exercise', 'myfitnessblog' ),
			'view_item'             => __( 'View exercise', 'myfitnessblog' ),
			'view_items'            => __( 'View exercise', 'myfitnessblog' ),
			'search_items'          => __( 'Search exercise', 'myfitnessblog' ),
			'not_found'             => __( 'Not found', 'myfitnessblog' ),
			'not_found_in_trash'    => __( 'Not found in trash', 'myfitnessblog' ),
			'featured_image'        => __( 'Featured image', 'myfitnessblog' ),
			'set_featured_image'    => __( 'Set featured image', 'myfitnessblog' ),
			'remove_featured_image' => __( 'Remove featured image', 'myfitnessblog' ),
			'use_featured_image'    => __( 'Use as featured image', 'myfitnessblog' ),
			'insert_into_item'      => __( 'Insert into exercise', 'myfitnessblog' ),
			'uploaded_to_this_item' => __( 'Uploaded to this exercise', 'myfitnessblog' ),
			'items_list'            => __( 'Exercises list', 'myfitnessblog' ),
			'items_list_navigation' => __( 'Exercises list navigation', 'myfitnessblog' ),
			'filter_items_list'     => __( 'Filter exercises list', 'myfitnessblog' ),
		];

		$args   = [
			'label'               => __( 'Exercise', 'myfitnessblog' ),
			'description'         => __( 'A post type to describe a single exercise', 'myfitnessblog' ),
			'labels'              => $labels,
			'supports'            => [
				'title',
				'editor',
				'thumbnail',
			],
			'taxonomies'          => [ 'sport_type', 'training_system' ],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 6,
			'menu_icon'           => 'dashicons-exerpt-view',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'post',
			'show_in_rest'        => false,
		];

		register_post_type( self::post_type, $args );
	}

	/**
	 * Add meta boxes in editor
	 *
	 * @codeCoverageIgnore
	 */
	public function add_meta_boxes() {
		add_meta_box( self::post_type, __( 'Example', 'myfitnessblog' ), [ $this, 'show_example' ], self::post_type, 'advanced', 'high' );
	}

	/**
	 * Actions to take when saving the post type
	 *
	 * @param $post_id
	 * @param $post
	 *
	 * @return int|bool
	 */
	public function save_post( $post_id, $post ) {
		$nonce = isset( $_POST['exercise_nonce'] ) ? $_POST['exercise_nonce'] : '';

		if ( ! wp_verify_nonce( $nonce, 'show_example' ) ) {
			return false;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return false;
		}

		$url = isset( $_POST['exercise_example_video'] ) ? esc_url( $_POST['exercise_example_video'] ) : '';

		return update_post_meta( $post_id, 'exercise_example_video', $url );
	}

	/**
	 * Show example
	 */
	public function show_example() {
		$post = get_post();

		wp_nonce_field( 'show_example', 'exercise_nonce' );

		echo '<table id="form-table">', PHP_EOL, '<tbody>';

		if ( $post ) {
			printf(
				'<tr><th>%1$s</th><td><input type="text" class="regular-text" name="exercise_example_video" value="%2$s" /></td></tr>',
				__( 'Video Link', 'myfitnessblog' ),
				get_post_meta( $post->ID, 'exercise_example_video', true )
			);
		}
		
		echo '</tbody>', PHP_EOL, '</table>', PHP_EOL;
	}

}