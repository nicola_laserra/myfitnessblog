<?php

namespace myfitnessblog;

/**
 * Class Options
 * @package myfitnessblog
 */
class Options {

	const name = 'myfitnessblog';

	protected $arr = [];

	/**
	 * Options constructor.
	 */
	public function __construct() {
		$this->arr = get_option( self::name, [] );
	}

	/**
	 * Options destructor updates option
	 */
	public function __destruct() {
		$this->update();
	}

	/**
	 * Factory
	 *
	 * @return Options
	 */
	public static function init() {
		return new self;
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 *
	 * @return Options
	 */
	public function set( $key, $value ) {
		$this->arr[ $key ] = $value;

		return $this;
	}

	/**
	 * @param string $key
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	public function get( $key, $default = false ) {
		return isset( $this->arr[ $key ] ) ? $this->arr[ $key ] : $default;
	}

	/**
	 * @param $name
	 *
	 * @return string
	 */
	protected function active_name( $name ) {
		return esc_attr( $name . '_active' );
	}

	/**
	 * @param $name
	 *
	 * @return bool
	 */
	public function is_active( $name ) {
		return boolval( $this->get( $this->active_name( $name ) ) );
	}

	/**
	 * @param $name
	 *
	 * @return Options
	 */
	public function set_active( $name ) {
		return $this->set( $this->active_name( $name ), true );
	}

	/**
	 * @return bool
	 */
	public function update() {
		return update_option( self::name, $this->arr );
	}

}