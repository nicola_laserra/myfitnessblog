<?php

namespace myfitnessblog;

/**
 * Class Plugin
 * @package myfitnessblog
 */
class Plugin {

	/**
	 * @var string
	 */
	protected $plugin_dir;

	/**
	 * Plugin constructor.
	 *
	 * @param string $plugin_dir
	 */
	public function __construct( $plugin_dir ) {
		$this->plugin_dir = $plugin_dir;
	}

	/**
	 * @return string
	 */
	public function get_plugin_dir() {
		return $this->plugin_dir;
	}

	/**
	 * Factory
	 *
	 * @codeCoverageIgnore
	 *
	 * @param string $plugin_dir
	 *
	 * @return Plugin
	 */
	public static function init( $plugin_dir ) {
		static $_instance = null;

		if ( is_null( $_instance) ) {
			$_instance = new self( $plugin_dir );

			add_action( 'plugins_loaded', [ $_instance, 'load_textdomain' ] );
			add_action( 'plugins_loaded', [ $_instance, 'init_classes' ] );
		}

		return $_instance;
	}

	/**
	 * Load plgin textdomain
	 *
	 * @codeCoverageIgnore
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'myfitnessblog', false, $this->plugin_dir );
	}

	/**
	 * Load Custom post types and custom taxonomies
	 *
	 * @codeCoverageIgnore
	 */
	public function init_classes() {
		Workout::init();
		Exercise::init();
		TrainingSystem::init();
		SportType::init();
	}

}