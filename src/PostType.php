<?php

namespace myfitnessblog;

/**
 * Class PostType
 * @package myfitnessblog
 */
abstract class PostType {

	/**
	 * Factory
	 *
	 * @codeCoverageIgnore
	 *
	 * @return Exercise
	 */
	public static function init() {
		$obj = new static;

		add_filter( 'enter_title_here', [ $obj, 'enter_title_here' ], 10, 2 );

		add_action( 'init', [ $obj, 'register' ], 0 );
		add_action( 'add_meta_boxes', [ $obj, 'add_meta_boxes' ] );
		add_action( 'save_post', [ $obj, 'save_post' ], 10, 2 );

		return $obj;
	}

	/**
	 * Change the placeholder title in the editor
	 *
	 * @param string $title
	 * @param \WP_Post $post
	 *
	 * @return string
	 */
	public function enter_title_here( $title, \WP_Post $post ) {
		if ( defined( 'static::post_type' ) && static::post_type == $post->post_type ) {
			return $this->get_title_here();
		}

		return $title;
	}

	/**
	 * Get placeholder title for a post type
	 *
	 * @return string
	 */
	abstract public function get_title_here();

	/**
	 * Register post-type
	 */
	abstract public function register();

	/**
	 * Add meta boxes in editor
	 */
	abstract public function add_meta_boxes();

	/**
	 * Actions to take when saving the post type
	 *
	 * @param $post_id
	 * @param $post
	 *
	 * @return int|bool
	 */
	abstract public function save_post( $post_id, $post );

}