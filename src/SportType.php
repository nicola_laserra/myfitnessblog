<?php

namespace myfitnessblog;

/**
 * Class SportType
 * @package myfitnessblog
 */
class SportType extends Taxonomy {

	const name = 'sport_type';

	/**
	 * Register taxonomy
	 *
	 * @codeCoverageIgnore
	 */
	public function register() {
		$labels = [
			'name'                       => _x( 'Types', 'Taxonomy General Name', 'myfitnessblog' ),
			'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'myfitnessblog' ),
			'menu_name'                  => __( 'Types', 'myfitnessblog' ),
			'all_items'                  => __( 'All Types', 'myfitnessblog' ),
			'parent_item'                => __( 'Parent Type', 'myfitnessblog' ),
			'parent_item_colon'          => __( 'Parent Type:', 'myfitnessblog' ),
			'new_item_name'              => __( 'New Type Name', 'myfitnessblog' ),
			'add_new_item'               => __( 'Add New Type', 'myfitnessblog' ),
			'edit_item'                  => __( 'Edit Type', 'myfitnessblog' ),
			'update_item'                => __( 'Update Type', 'myfitnessblog' ),
			'view_item'                  => __( 'View Type', 'myfitnessblog' ),
			'separate_items_with_commas' => __( 'Separate types with commas', 'myfitnessblog' ),
			'add_or_remove_items'        => __( 'Add or remove types', 'myfitnessblog' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'myfitnessblog' ),
			'popular_items'              => __( 'Popular Types', 'myfitnessblog' ),
			'search_items'               => __( 'Search Types', 'myfitnessblog' ),
			'not_found'                  => __( 'Not Found', 'myfitnessblog' ),
			'no_terms'                   => __( 'No types', 'myfitnessblog' ),
			'items_list'                 => __( 'Types list', 'myfitnessblog' ),
			'items_list_navigation'      => __( 'Types list navigation', 'myfitnessblog' ),
		];

		$args = [
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		];

		register_taxonomy( self::name, [ 'workout', 'exercise' ], $args );

		$this->maybe_insert_terms( [ 'Bodyweight', 'Free Weights' ], self::name );
	}

}