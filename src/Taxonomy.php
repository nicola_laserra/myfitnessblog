<?php

namespace myfitnessblog;

/**
 * Class Taxonomy
 * @package myfitnessblog
 */
abstract class Taxonomy {

	protected $options;

	/**
	 * Taxonomy constructor.
	 *
	 * @param Options $options
	 */
	public function __construct( Options $options ) {
		$this->options = $options;
	}

	/**
	 * Factory
	 *
	 * @codeCoverageIgnore
	 * 
	 * @return Taxonomy
	 */
	public static function init() {
		$obj = new static( Options::init() );

		add_action( 'init', [ $obj, 'register' ], 0 );

		return $obj;
	}

	abstract public function register();

	/**
	 * @param array $arr
	 * @param string $taxonomy
	 *
	 * @return bool
	 */
	public function maybe_insert_terms( array $arr, $taxonomy ) {
		$taxonomy = esc_attr( $taxonomy );

		if ( ! $this->options->is_active( $taxonomy ) ) {
			foreach ( $arr as $type ) {
				if ( ! term_exists( $type, $taxonomy ) ) {
					wp_insert_term( $type, $taxonomy );
				}
			}

			$this->options->set_active( $taxonomy );

			return true;
		}

		return false;
	}

}