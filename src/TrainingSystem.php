<?php

namespace myfitnessblog;

/**
 * Class TrainingSystem
 * @package myfitnessblog
 */
class TrainingSystem extends Taxonomy {

	const name = 'training_system';

	/**
	 * Register taxonomy
	 *
	 * @codeCoverageIgnore
	 */
	public function register() {
		$labels = [
			'name'                       => _x( 'Systems', 'Taxonomy General Name', 'myfitnessblog' ),
			'singular_name'              => _x( 'System', 'Taxonomy Singular Name', 'myfitnessblog' ),
			'menu_name'                  => __( 'Systems', 'myfitnessblog' ),
			'all_items'                  => __( 'All Systems', 'myfitnessblog' ),
			'parent_item'                => __( 'Parent System', 'myfitnessblog' ),
			'parent_item_colon'          => __( 'Parent System:', 'myfitnessblog' ),
			'new_item_name'              => __( 'New System Name', 'myfitnessblog' ),
			'add_new_item'               => __( 'Add New System', 'myfitnessblog' ),
			'edit_item'                  => __( 'Edit System', 'myfitnessblog' ),
			'update_item'                => __( 'Update System', 'myfitnessblog' ),
			'view_item'                  => __( 'View System', 'myfitnessblog' ),
			'separate_items_with_commas' => __( 'Separate systems with commas', 'myfitnessblog' ),
			'add_or_remove_items'        => __( 'Add or remove systems', 'myfitnessblog' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'myfitnessblog' ),
			'popular_items'              => __( 'Popular Systems', 'myfitnessblog' ),
			'search_items'               => __( 'Search Systems', 'myfitnessblog' ),
			'not_found'                  => __( 'Not Found', 'myfitnessblog' ),
			'no_terms'                   => __( 'No systems', 'myfitnessblog' ),
			'items_list'                 => __( 'Systems list', 'myfitnessblog' ),
			'items_list_navigation'      => __( 'Systems list navigation', 'myfitnessblog' ),
		];

		$args = [
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
		];

		register_taxonomy( self::name, [ 'workout', 'exercise' ], $args );

		$this->maybe_insert_terms( [ 'Freeletics', 'Runtastic' ], self::name );
	}

}