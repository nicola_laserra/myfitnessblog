<?php

namespace myfitnessblog;

/**
 * Class Workout
 *
 * @package myfitnessblog
 */
class Workout extends PostType {

	const post_type = 'workout';

	/**
	 * Get place holder title 
	 */
	public function get_title_here() {
		return __( 'Insert workout title here', 'myfitnessblog' );
	}

	/**
	 * Register post type
	 *
	 * @codeCoverageIgnore
	 */
	public function register() {
		$labels = [
			'name'                  => _x( 'Workouts', 'Post Type General Name', 'myfitnessblog' ),
			'singular_name'         => _x( 'Workout', 'Post Type Singular Name', 'myfitnessblog' ),
			'menu_name'             => __( 'Workouts', 'myfitnessblog' ),
			'name_admin_bar'        => __( 'Workout', 'myfitnessblog' ),
			'archives'              => __( 'Workout archives', 'myfitnessblog' ),
			'attributes'            => __( 'Workout attributes', 'myfitnessblog' ),
			'parent_item_colon'     => __( 'Parent item:', 'myfitnessblog' ),
			'all_items'             => __( 'All workouts', 'myfitnessblog' ),
			'add_new_item'          => __( 'Add new workout', 'myfitnessblog' ),
			'add_new'               => __( 'Add new', 'myfitnessblog' ),
			'new_item'              => __( 'New workout', 'myfitnessblog' ),
			'edit_item'             => __( 'Edit workout', 'myfitnessblog' ),
			'update_item'           => __( 'Update workout', 'myfitnessblog' ),
			'view_item'             => __( 'View workout', 'myfitnessblog' ),
			'view_items'            => __( 'View workout', 'myfitnessblog' ),
			'search_items'          => __( 'Search workout', 'myfitnessblog' ),
			'not_found'             => __( 'Not found', 'myfitnessblog' ),
			'not_found_in_trash'    => __( 'Not found in trash', 'myfitnessblog' ),
			'featured_image'        => __( 'Featured image', 'myfitnessblog' ),
			'set_featured_image'    => __( 'Set featured image', 'myfitnessblog' ),
			'remove_featured_image' => __( 'Remove featured image', 'myfitnessblog' ),
			'use_featured_image'    => __( 'Use as featured image', 'myfitnessblog' ),
			'insert_into_item'      => __( 'Insert into workout', 'myfitnessblog' ),
			'uploaded_to_this_item' => __( 'Uploaded to this workout', 'myfitnessblog' ),
			'items_list'            => __( 'Workouts list', 'myfitnessblog' ),
			'items_list_navigation' => __( 'Workouts list navigation', 'myfitnessblog' ),
			'filter_items_list'     => __( 'Filter workouts list', 'myfitnessblog' ),
		];

		$args = [
			'label'               => __( 'Workout', 'myfitnessblog' ),
			'description'         => __( 'A post type to describe a complete workout', 'myfitnessblog' ),
			'labels'              => $labels,
			'supports'            => [
				'title',
				'editor',
				'thumbnail',
			],
			'taxonomies'          => [ 'sport_type', 'training_system' ],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 7,
			'menu_icon'           => 'dashicons-list-view',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'show_in_rest'        => true,
		];

		register_post_type( self::post_type, $args );
	}

	/**
	 * Add meta boxes in editor
	 *
	 * @codeCoverageIgnore
	 */
	public function add_meta_boxes() {
		add_meta_box( self::post_type, __( 'Exercises', 'myfitnessblog' ), [
			$this,
			'show_exercises'
		], self::post_type, 'advanced', 'high' );
	}

	/**
	 * Actions to take when saving the post type
	 *
	 * @param $post_id
	 * @param $post
	 *
	 * @return int|bool
	 */
	public function save_post( $post_id, $post ) {
		$nonce = isset( $_POST['workout_nonce'] ) ? $_POST['workout_nonce'] : '';

		if ( ! wp_verify_nonce( $nonce, 'show_exercises' ) ) {
			return false;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return false;
		}

		return update_post_meta( $post_id, 'workoutDetails', $this->filter( $_POST ) );
	}

	/**
	 * Show metabox in editor
	 */
	public function show_exercises() {
		global $post;

		wp_nonce_field( 'show_exercises', 'workout_nonce' );

		echo '<table class="form-table">', PHP_EOL;

		$tr = sprintf(
			'<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
			__( 'Reps', 'myfitnessblog' ),
			__( 'Time in min', 'myfitnessblog' ),
			__( 'Weight in kg', 'myfitnessblog' ),
			__( 'Exercise', 'myfitnessblog' )
		);

		printf( '<thead>%s</thead>', $tr );
		echo PHP_EOL;

		printf( '<tfoot>%s</tfoot>', $tr );
		echo PHP_EOL;

		echo '<tbody>', PHP_EOL;

		$details = get_post_meta( $post->ID, 'workoutDetails', true );
		$counter = 0;
		if ( is_array( $details ) && count( $details ) > 0 ) {
			foreach ( $details as $detail ) {
				echo $this->get_detail( $counter, $detail );

				$counter += 1;
			}
		}

		echo $this->get_detail( $counter );

		echo '</tbody>', PHP_EOL, '</table>', PHP_EOL;
	}

	/**
	 * @param int|string $counter
	 * @param array $detail
	 *
	 * @return string
	 */
	public function get_detail( $counter, array $detail = [ ] ) {
		$counter = intval( $counter );

		foreach ( [ 'reps', 'time', 'weight', 'exercise' ] as $field ) {
			$detail[ $field ] = ! empty( $detail[ $field ] ) ? intval( $detail[ $field ] ) : '';
		}

		return sprintf(
			'<tr><td><input class="small-text" name="workoutDetails[%1$d][reps]" value="%2$s"></td><td><input class="small-text" name="workoutDetails[%1$d][time]" value="%3$s"></td><td><input class="small-text" name="workoutDetails[%1$d][weight]" value="%4$s"></td><td><select name="workoutDetails[%1$d][exercise]">%5$s</select></td></tr>',
			intval( $counter ),
			$detail['reps'],
			$detail['time'],
			$detail['weight'],
			$this->get_options( $detail['exercise'] )
		);
	}

	/**
	 * @param int $exercise
	 *
	 * @return string
	 */
	public function get_options( $exercise ) {
		static $options = null;

		if ( is_null( $options ) ) {
			$args = [
				'posts_per_page' => - 1,
				'orderby'        => 'title',
				'post_type'      => 'exercise',
				'post_status'    => 'publish',
			];

			$posts = get_posts( $args );
			if ( $posts ) {
				foreach ( $posts as $option ) {
					$options[ $option->ID ] = esc_attr( $option->post_title );
				};
			}
		}

		$html = sprintf( '<option value="0">%s</option>', __( '--- Select ---', 'myfitnessblog' ) );

		if ( $options ) {
			foreach ( $options as $key => $title ) {
				$html .= sprintf( '<option value="%1$s" %2$s>%3$s</option>', $key, selected( $key, $exercise, false ), $title );
			}
		}
		
		return $html;
	}

	/**
	 * @param array $raw
	 *
	 * @return array
	 */
	public function filter( $raw ) {
		$arr = [ ];

		if ( isset( $raw['workoutDetails'] ) ) {
			foreach ( $raw['workoutDetails'] as $key => $values ) {
				$check = array_filter( $values );
				if ( count( $check ) > 0 ) {
					$arr[ $key ] = $values;
				}
			}
		}

		return $arr;
	}

}