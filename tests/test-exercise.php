<?php
/**
 * Class ExerciseTest
 *
 * @package Myfitnessblog
 */

/**
 * Class ExerciseTest
 */
class ExerciseTest extends WP_UnitTestCase {

	public function test_get_title_here() {
		$exercise = new myfitnessblog\Exercise();
		$this->assertInternalType( 'string', $exercise->get_title_here() );
	}

	public function test_enter_title_here() {
		$exercise = new myfitnessblog\Exercise();
		$post     = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post' ] );

		$this->assertEmpty( $exercise->enter_title_here( '', $post ) );

		$post     = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post', 'post_type' => 'exercise' ] );

		$this->assertNotEmpty( $exercise->enter_title_here( '', $post ) );
	}

	public function test_save_post() {
		$exercise = new myfitnessblog\Exercise();

		$post     = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post' ] );

		$this->assertFalse( $exercise->save_post( $post->ID, $post ) );
	}

	public function test_show_example() {
		$exercise = new myfitnessblog\Exercise();

		$this->expectOutputRegex( '#tbody.*tbody.*#' );
		$exercise->show_example();
	}
}
