<?php
/**
 * Class OptionsTest
 *
 * @package Myfitnessblog
 */

/**
 * Class OptionsTest
 */
class OptionsTest extends WP_UnitTestCase {

	public function test_git() {
		$options = myfitnessblog\Options::init();

		$this->assertFalse( $options->get( 'test' ) );

		$options->set( 'test', 123 );
		$this->assertEquals( 123, $options->get( 'test' ) );

		$options->set_active( 'test' );
		$this->assertTrue( $options->is_active( 'test' ) );

		$options = myfitnessblog\Options::init();

		$this->assertTrue( $options->update() );
	}

}
