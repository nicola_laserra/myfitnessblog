<?php
/**
 * Class PluginTest
 *
 * @package Myfitnessblog
 */

/**
 * Class PluginTest
 */
class PluginTest extends WP_UnitTestCase {

	function test_get_plugin_dir() {
		$plugin = new myfitnessblog\Plugin( __FILE__ );

		$this->assertInternalType( 'string', $plugin->get_plugin_dir() );
	}

}
