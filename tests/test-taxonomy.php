<?php
/**
 * Class TaxonomyTest
 *
 * @package Myfitnessblog
 */

/**
 * Class TaxonomyTest
 */
class TaxonomyTest extends WP_UnitTestCase {

	function test_maybe_insert_terms() {
		$option = $this->createMock( myfitnessblog\Options::class );
		$option->method( 'is_active' )->will( $this->onConsecutiveCalls( false, true ) );

		$taxonomy = $this->getMockForAbstractClass( myfitnessblog\Taxonomy::class, [ $option ] );
		$taxonomy->expects( $this->any() )->method( 'register' )->will( $this->returnValue( null ) );

		$this->assertTrue( $taxonomy->maybe_insert_terms( [ 'abc', 'def' ], 'training_system' ) );
		$this->assertFalse( $taxonomy->maybe_insert_terms( [ 'abc', 'def' ], 'training_system' ) );
	}

}
