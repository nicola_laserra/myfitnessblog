<?php
/**
 * Class WorkoutTest
 *
 * @package Myfitnessblog
 */

/**
 * Class WorkoutTest
 */
class WorkoutTest extends WP_UnitTestCase {

	function test_get_title_here() {
		$workout = new myfitnessblog\Workout();
		$this->assertInternalType( 'string', $workout->get_title_here() );
	}

	function test_enter_title_here() {
		$workout = new myfitnessblog\Workout();
		$post    = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post' ] );

		$this->assertEmpty( $workout->enter_title_here( '', $post ) );

		$post    = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post', 'post_type' => 'workout' ] );

		$this->assertNotEmpty( $workout->enter_title_here( '', $post ) );
	}

	public function test_save_post() {
		$post     = $this->factory->post->create_and_get( [ 'post_title' => 'Test Post' ] );

		$workout = new myfitnessblog\Workout();

		$this->assertFalse( $workout->save_post( $post->ID, $post ) );
	}

	public function test_get_options() {
		$workout = new myfitnessblog\Workout();

		$expected = '<option value="0">--- Select ---</option>';
		$this->assertEquals( $expected, $workout->get_options( 2 ) );
	}

	public function test_filter() {
		$input    = [ 'workoutDetails' => [ '1' => [ 'reps' => 1 ], '2' => [ 'reps' => '' ] ] ];
		$expected = [ '1' => [ 'reps' => 1 ] ];

		$workout = new myfitnessblog\Workout();

		$this->assertEquals( $expected, $workout->filter( $input ) );
	}

}
